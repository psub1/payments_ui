import axios from "axios";
import environment from '../environment'

export const findPaymentBegin = (type, input) => {
    return {
        type: "FIND_PAYMENT_BEGIN",
        payload: `Finding payments for ${type} ${input}..`
    };
};

export const findPaymentSuccess = (payment) => {
    return {
        type: "FIND_PAYMENT_SUCCESS",
        payload: payment,
    };
};

export const findPaymentFailure = (error) => {
    return {
        type: "FIND_PAYMENT_FAILURE",
        payload: { message: "Failed to find payment! Please try again later..", ...error },
    };
};

export const findPayment = (type, input) => {

    return (dispatch, getState) => {

        if (!input) {
            dispatch(findPaymentFailure({ message: "Please enter a valid input.." }));
            return Promise.resolve();
        }

        switch (type) {
            case "id":
                dispatch(findPaymentBegin(type, input));
                return axios.get(environment.apiPath + "/findById/" + input.trim()).then(
                    (res) => {
                        if (res.data) {
                            dispatch(findPaymentSuccess([res.data]));
                        } else {
                            dispatch(findPaymentFailure({ message: "No results found! Please try with different input.." }));
                        }
                    },
                    (err) => {
                        dispatch(findPaymentFailure(err));
                    }
                );
            case "type":
                dispatch(findPaymentBegin(type, input));
                return axios.get(environment.apiPath + "/findByType/" + input.trim()).then(
                    (res) => {
                        if (res.data && res.data.length) {
                            dispatch(findPaymentSuccess(res.data));
                        } else {
                            dispatch(findPaymentFailure({ message: "No results found! Please try with different input.." }));
                        }
                    },
                    (err) => {
                        dispatch(findPaymentFailure(err));
                    }
                );
            default:
                dispatch(findPaymentFailure({ message: "Invalid input! Please enter a valid input.." }));
                return Promise.resolve();
        }
    };
};

export const findAllPayments = () => {
    return (dispatch, getState) => {
        dispatch(findPaymentBegin("all", "types"));
        return axios.get(environment.apiPath + "/findAll").then(
            (res) => {
                dispatch(findPaymentSuccess(res.data));
            },
            (err) => {
                // Not throwing error as find all is optional
                // API endpoint may not be avaialble as it was not in requirements
                dispatch(findPaymentSuccess([]));
            }
        );
    };
};
