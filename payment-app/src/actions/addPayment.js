import axios from "axios";
import environment from '../environment'

export const addPaymentBegin = (payment) => {
    return {
        type: "ADD_PAYMENT_BEGIN",
        payload: `Adding new payment of $${payment.amount}..`
    };
};

export const addPaymentSuccess = (id) => {
    return {
        type: "ADD_PAYMENT_SUCCESS",
        payload: id
    };
};

export const addPaymentFailure = (error) => {
    return {
        type: "ADD_PAYMENT_FAILURE",
        payload: { message: "Payment addition failed! Please try again later..", ...error },
    };
};

export const addPayment = (payment) => {

    return (dispatch, getState) => {
        dispatch(addPaymentBegin(payment));
        return axios.post(environment.apiPath + "/save", payment).then(
            (res) => {
                if (res.data > 0) {
                    dispatch(addPaymentSuccess(res.data));
                } else {
                    dispatch(addPaymentFailure({ message: "Failed to add new payment! Please try again later.." }))
                }
            },
            (err) => {
                dispatch(addPaymentFailure(err));
            }
        );
    };
};

export const clearAddPayment = () => {
    return (dispatch, getState) => {
        dispatch(addPaymentSuccess(0));
    };
};
