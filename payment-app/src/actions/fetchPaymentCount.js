import axios from "axios";
import environment from '../environment'

export const fetchPaymentCountBegin = () => {
    return {
        type: "FETCH_PAYMENT_COUNT_BEGIN"
    };
};

export const fetchPaymentCountSuccess = (count) => {
    return {
        type: "FETCH_PAYMENT_COUNT_SUCCESS",
        payload: parseInt(count),
    };
};

export const fetchPaymentCountFailure = (error) => {
    return {
        type: "FETCH_PAYMENT_COUNT_FAILURE",
        payload: -1,
    };
};

export const fetchPaymentCount = () => {

    return (dispatch, getState) => {
        dispatch(fetchPaymentCountBegin());
        return axios.get(environment.apiPath + "/rowcount").then(
            (res) => {
                dispatch(fetchPaymentCountSuccess(res.data));
            },
            (err) => {
                dispatch(fetchPaymentCountFailure(err));
            }
        );
    };
};
