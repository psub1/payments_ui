import axios from "axios";
import environment from '../environment'

export const fetchStatusSuccess = (status) => {
    return {
        type: "FETCH_STATUS_SUCCESS"
    };
};

export const fetchStatusFailure = (err) => {
    return {
        type: "FETCH_STATUS_FAILURE"
    };
};

export const observeStatus = () => {
    return (dispatch, getState) => {
        getStatus(dispatch, getState);
        setInterval(getStatus, 7000, dispatch, getState);
    };
};

const getStatus = (dispatch, getState) => {

    return axios.get(environment.apiPath + "/status").then(
        (res) => {
            dispatch(fetchStatusSuccess(res.data));
        },
        (err) => {
            dispatch(fetchStatusFailure(err));
        }
    );
};
