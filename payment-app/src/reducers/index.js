import { combineReducers } from 'redux';
import paymentsReducer from './paymentReducer';
import statusReducer from './statusReducer';

const rootReducer = combineReducers({
    payment: paymentsReducer,
    status: statusReducer
});

export default rootReducer;
