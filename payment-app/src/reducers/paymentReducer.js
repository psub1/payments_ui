const initialState = {};

export default (state = initialState, action) => {
    console.log(`Received ${action.type} dispatched in paymentsReducer`);
    switch (action.type) {
        case "FIND_PAYMENT_BEGIN":
        case "ADD_PAYMENT_BEGIN":
            return { ...state, loading: true, message: action.payload };
        case "FIND_PAYMENT_SUCCESS":
            return { ...state, loading: false, entities: action.payload, error: null };
        case "FIND_PAYMENT_FAILURE":
        case "ADD_PAYMENT_FAILURE":
            return { ...state, loading: false, error: action.payload };
        case "ADD_PAYMENT_SUCCESS":
            return { ...state, loading: false, paymentid: action.payload, error: null };
        case "FETCH_PAYMENT_COUNT_BEGIN":
            return state;
        case "FETCH_PAYMENT_COUNT_SUCCESS":
            return { ...state, rowcount: action.payload }
        case "FETCH_PAYMENT_COUNT_FAILURE":
            return { ...state, rowcount: action.payload }
        default:
            return state;
    }
};
