
export default (state = false, action) => {
    console.log(`Received ${action.type} dispatch in statusReducer`);

    switch (action.type) {
        case "FETCH_STATUS_SUCCESS":
            return true;
        case "FETCH_STATUS_FAILURE":
            return false;
        default:
            return state;
    }
};
