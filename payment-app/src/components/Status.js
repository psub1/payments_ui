import '../styles/components/Status.css';
import online from '../images/online.png';
import offline from '../images/offline.png';
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { observeStatus } from '../actions/fetchStatus';

const Status = () => {
    const status = useSelector((state) => state.status);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(observeStatus());
    }, []);

    return (
        <div className="status text-right" title={status ? "API online" : "API offline"}>
            <img className="status-img" src={status ? online : offline} />
                    PayApp v1.0.1
        </div>
    );
};

export default Status;
