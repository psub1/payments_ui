import '../../styles/components/payment-app/Error.css';
import React from 'react';

const Error = (props) => {
    return (
        <div className="d-flex justify-content-center error">
            {props.message}
        </div>
    );
};

export default Error;
