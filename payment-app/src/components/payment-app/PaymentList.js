import '../../styles/components/payment-app/PaymentList.css';
import React from 'react';

const PaymentList = (props) => {

    return (
        <div className="container payment-list">
            <div id="accordion">
                {
                    props.payments.map(payment => (
                        <div key={payment.id} className="card">
                            <div className="card-header">
                                <h5 className="mb-0">
                                    <button className="btn btn-link" data-toggle="collapse" aria-controls="collapseOne" data-target={"#collapse" + payment.id} aria-controls={"collapse" + payment.id}>
                                        {"Payment of $" + payment.amount}
                                    </button>
                                </h5>
                            </div>
                            <div id={"collapse" + payment.id} className="collapse">
                                <div className="card-body">
                                    Payment id: <strong>{payment.id}</strong>, type: <strong>{payment.type}</strong>, made by customer id <strong>{payment.custid}</strong> on <strong>{payment.paymentdate}</strong>
                                </div>
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>
    );
};

export default PaymentList;
