import '../../styles/components/payment-app/Banner.css';
import React from 'react';

const Banner = () => {
    return (
        <div className="jumbotron jumbotron-fluid text-center banner">
            <div className="container">
                <h2 className="display-6">PayApp</h2>
                <hr className="my-1" />
                <p className="lead">This is an application for managing Payments</p>
                <a className="btn btn-outline-primary" href="https://bitbucket.org/psub1/uiproject_pasupathi/src" target="_blank" role="button">Get SourceCode</a>
            </div>
        </div>
    );
};

export default Banner;
