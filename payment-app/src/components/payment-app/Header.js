import '../../styles/components/payment-app/Header.css';
import brand from '../../images/brand.png';
import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => {
    return (
        <nav className="navbar sticky-top navbar-expand-md navbar-dark bg-primary header">
            <a href="#" className="navbar-brand">
                <img src={brand} alt="PayApp" />
            </a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink exact className="nav-link" to="/" activeClassName="active">
                            Home
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink exact className="nav-link" to="/add" activeClassName="active">
                            Add Payment
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink exact className="nav-link" to="/about" activeClassName="active">
                            About
                        </NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    );
};

export default Header;
