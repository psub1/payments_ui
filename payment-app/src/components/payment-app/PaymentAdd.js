import '../../styles/components/payment-app/PaymentAdd.css';
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { addPayment, clearAddPayment } from '../../actions/addPayment';
import { fetchPaymentCount } from '../../actions/fetchPaymentCount';
import Spinner from './Spinner';
import Error from './Error';

const PaymentAdd = () => {
    const payment = useSelector(state => state.payment);
    const [amount, setAmount] = useState(0.00);
    const [type, setType] = useState("");
    const [custid, setCustid] = useState(0);
    const [paymentdate, setPaymentdate] = useState("");
    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        dispatch(fetchPaymentCount());
        dispatch(clearAddPayment());
    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();

        let dateInput = new Date(paymentdate);
        let date = dateInput.getDate().toString();
        date = (date.length > 1 ? date : "0" + date);
        let month = (dateInput.getMonth() + 1).toString();
        month = (month.length > 1 ? month : "0" + month);
        let year = dateInput.getFullYear().toString();
        let hours = dateInput.getHours().toString();
        hours = (hours.length > 1 ? hours : "0" + hours);
        let minutes = dateInput.getMinutes().toString();
        minutes = (minutes.length > 1 ? minutes : "0" + minutes);
        let seconds = dateInput.getSeconds().toString()
        seconds = (seconds.length > 1 ? seconds : "0" + seconds);

        dispatch(addPayment({
            id: payment.rowcount + 1,
            amount: amount,
            type: type,
            custid: custid,
            paymentdate: `${date}/${month}/${year} ${hours}:${minutes}:${seconds}`
        }))
            .finally(() => {
                console.log("Add payment dispath finished!");
                setTimeout(() => {
                    history.push("/");
                }, 2000);
            });
    };

    if (payment.loading) {
        return (
            <Spinner message={payment.message ? payment.message : "Loading..."} />
        );
    }

    if (payment.error) {
        return (
            <Error message={payment.error.message} />
        );
    }

    if (payment.paymentid > 0) {
        return (
            <div className="d-flex justify-content-center text-success">
                New payment added successfully with id {payment.paymentid} !!
            </div>
        );
    }

    return (
        <div className="container payment-add">
            <div className="container">
                <p className="lead">Add Payment</p>
                <hr className="my-1" />
            </div>
            <div className="container">
                <form onSubmit={handleSubmit} autoComplete="off">
                    <div className="form-row justify-content-center">
                        <div className="form-group col-md-5">
                            <label htmlFor="amount">Amount</label>
                            <input
                                id="amount"
                                type="number"
                                step="0.01"
                                min="1"
                                required
                                className="form-control"
                                value={amount}
                                onChange={(e) => setAmount(e.target.value)}
                            />
                        </div>
                        <div className="form-group col-md-5">
                            <label htmlFor="type">Type</label>
                            <input
                                id="type"
                                type="text"
                                required
                                className="form-control"
                                value={type}
                                onChange={(e) => setType(e.target.value)}
                            />
                        </div>
                    </div>
                    <div className="form-row justify-content-center">
                        <div className="form-group col-md-5">
                            <label htmlFor="custid">Customer Id</label>
                            <input
                                id="custid"
                                type="number"
                                min="2001"
                                required
                                className="form-control"
                                value={custid}
                                onChange={(e) => setCustid(e.target.value)}
                            />
                        </div>
                        <div className="form-group col-md-5">
                            <label htmlFor="paymentdate">Payment Date</label>
                            <input
                                id="tracks"
                                type="datetime-local"
                                required
                                className="form-control"
                                value={paymentdate}
                                onChange={(e) => setPaymentdate(e.target.value)}
                            />
                        </div>
                    </div>
                    <div className="d-flex justify-content-center">
                        <button type="submit" className="btn btn-outline-primary">Add payment</button>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default PaymentAdd;
