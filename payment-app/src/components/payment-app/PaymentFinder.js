import '../../styles/components/payment-app/PaymentFinder.css';
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Spinner from './Spinner';
import Error from './Error';
import PaymentList from './PaymentList';
import { findPayment, findAllPayments } from '../../actions/findPayment';
import { fetchPaymentCount } from '../../actions/fetchPaymentCount';

const PaymentFinder = () => {
    const payment = useSelector(state => state.payment);
    const dispatch = useDispatch();
    const [input, setInput] = useState("");
    const [method, setMethod] = useState("id");
    let content;

    useEffect(() => {
        dispatch(fetchPaymentCount());
        dispatch(findAllPayments());
    }, []);

    const handleSubmit = (event) => {
        event.preventDefault();
        dispatch(findPayment(method, input))
            .finally(() => {
                console.log("Find payments dispatch finished!");
            });
    };

    if (payment.loading) {
        content = (
            <Spinner message={payment.message ? payment.message : "Loading payments..."} />
        );
    } else if (payment.error) {
        content = (
            <Error message={payment.error.message} />
        );
    } else if (payment.entities && payment.entities.length) {
        content = (
            <React.Fragment>
                <div className="d-flex justify-content-center">
                    {payment.rowcount > 0 ? <p>Showing {payment.entities.length} out of {payment.rowcount} payments</p> : null}
                </div>
                <PaymentList payments={payment.entities} />
            </React.Fragment>
        );
    }

    return (
        <div className="container mt-3 payment-finder">
            <div className="container">
                <p className="lead">Find Payment</p>
                <hr className="my-1" />
            </div>
            <div className="d-flex justify-content-center">
                <form onSubmit={handleSubmit}>
                    <div className="input-group mt-3 mb-3">
                        <input type="text" onChange={e => setInput(e.target.value)} className="form-control" placeholder={method === "id" ? "Enter an id" : "Enter type"} />
                        <div className="input-group-append">
                            <button type="submit" className="btn btn-outline-primary">
                                {method === "id" ? "Find by Id" : "Find by Type"}
                            </button>
                            <button type="button" className="btn btn-outline-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span className="sr-only">Toggle Dropdown</span>
                            </button>
                            <div className="dropdown-menu">
                                <a className="dropdown-item" onClick={e => { setMethod("id"); return false; }} >Find by Id</a>
                                <a className="dropdown-item" onClick={e => { setMethod("type"); return false; }} >Find by Type</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            {content}
        </div>
    );
};

export default PaymentFinder;
