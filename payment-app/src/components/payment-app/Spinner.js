import '../../styles/components/payment-app/Spinner.css';
import React from 'react';

const Spinner = (props) => {
    return (
        <div className="d-flex flex-column align-items-center justify-content-center">
            <div className="row">
                <div className="spinner-border text-primary spinner" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
            <div className="row">
                {props.message ? props.message : "Loading..."}
            </div>
        </div>
    );
};

export default Spinner;
