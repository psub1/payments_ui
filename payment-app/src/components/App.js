import '../styles/components/App.css';
import Header from './payment-app/Header';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Banner from './payment-app/Banner';
import Status from './Status';
import PaymentFinder from './payment-app/PaymentFinder';
import About from './About';
import Footer from './payment-app/Footer';
import PaymentAdd from './payment-app/PaymentAdd';

function App() {

  return (
    <Router>
      <div className="app">
        <Header />
        <Banner />
        <Switch>
          <Route exact path="/">
            <PaymentFinder />
          </Route>
          <Route path="/add">
            <PaymentAdd />
          </Route>
          <Route path="/about">
            <About />
          </Route>
        </Switch>
        <Footer />
        <Status />
      </div>
    </Router>
  );
}

export default App;
