import '../styles/components/About.css';
import online from '../images/online.png';
import offline from '../images/offline.png';
import React from 'react';
import { useSelector } from 'react-redux';

const About = () => {
    const status = useSelector((state) => state.status);

    return (
        <div className="container text-center">
            PayApp v1.0.1
            <br />
            Author: psub1
            <br />
            Technology: React
            <br />
            <img className="status-img-big" src={status ? online : offline} />
            {status ? "Payment API is online" : "Payment API is offline"}
        </div>
    );
};

export default About;
